package com.testproject.testcases;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;

import org.testng.annotations.Test;

public class TestCase1 {
	int ID;
	
	@Test(priority = 1)
	void getUsers() {
		given().when().get("https://reqres.in/api/users/" + 2).then().statusCode(200)
				.log().all();
	}

	@Test(priority = 2)
	void createUser() {
		HashMap data = new HashMap();
		data.put("name", "Manasi");
		data.put("job", "SDE");

		ID = given().contentType("application/json").body(data).when().post("https://reqres.in/api/users")
//		Below code will return the id and store it in ID variable
				.jsonPath().getInt("id");
//		.then()
//		.log().all();
	}

	

	@Test(priority = 3, dependsOnMethods = { "createUser" })
	void updateUser() {

		HashMap data = new HashMap();
		data.put("name", "Manasi");
		data.put("job", "BA");

		given().contentType("application/json").body(data).when().put("https://reqres.in/api/users/" + ID).then()
				.statusCode(200).log().all();

	}

	@Test(priority = 4)
	void deleteUser() {
		given().when().delete("https://reqres.in/api/users/" + ID).then().statusCode(204);
	}
}
